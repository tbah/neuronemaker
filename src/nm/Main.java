/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nm;

import java.util.Locale;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;
import static nm.Nm_Constants.APP_TITLE;
import nm.ui.Gui;
import nm.ui.Options;
/**
 *
 * @author Thierno
 */
public class Main extends Application{
    Gui ui;
    @Override
    public void start(Stage primaryStage){
        ui = new Gui(primaryStage);
        ui.initGUI(APP_TITLE);
        
    }
    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        launch(args);
    }
}

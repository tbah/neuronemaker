/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nm;

/**
 *
 * @author Thierno
 */
public class Nm_Constants {

    public static final String PROPERTIES_FILE_NAME = "properties.xml";
    public static final String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";
    public static final String PATH_DATA = "./data/";

    public static final String PATH_DRAFT = PATH_DATA + "Draft/";
    public static final String PATH_IMAGES = "./images/";
    public static final String PATH_ARTWORK = "./Artwork/";
    public static final String PATH_CSS = "nm/css/";
    public static final String PATH_SITES = "sites/";
    public static final String PATH_BASE = PATH_SITES + "base/";
    public static final String PATH_EMPTY = ".";
    public static final String APP_TITLE = "Neurone Maker";
    public static final String VOWEL = "Vowel";
    public static final String CONSONANT = "Consonant";
    public static final String ENTER = "Enter";
    public static final String ALPHABET = "Letters";
    public static final String DIGIT = "Numbers";
    public static final String ALPHABET_TOOLTIP = "Letters for next round";
    public static final String DIGIT_TOOLTIP = "numbers for next round";
}

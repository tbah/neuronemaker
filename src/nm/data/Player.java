/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nm.data;

/**
 *
 * @author Thierno
 */
public class Player {

    String name;
    int score;

    public Player() {
        name = "Player";
        score = 0;
    }

    public Player(String p) {
        name = p;
        score = 0;
    }

    public void setName(String s) {
        name = s;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public void addScore(int n) {
        score += n;
    }

}

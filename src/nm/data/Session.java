/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nm.data;

import static nm.data.Mode.*;


/**
 *
 * @author Thierno
 */
public class Session {
    private int numberOfPlayer;
    private Player[] players;
    private int playerCounter;
    private boolean alternate;
    private Mode mode;
    public Session(){
        numberOfPlayer = 1;
        playerCounter = 0;
        players = new Player[numberOfPlayer];
        alternate = false;
        mode = LETTER;
    }
    public Session(int n, Mode m){
        playerCounter = 0;
        numberOfPlayer = n;
        players = new Player[numberOfPlayer];
        alternate = false;
        mode = m;
    }
    public void setNumberPlayer(int n){
       numberOfPlayer = n; 
    }
    public void setAlternate(boolean status){
        alternate = status;
    }
    public boolean getAlernate(){
        return alternate;
    }
    public int getNumberPlayer(){
        return numberOfPlayer;
    }
    public void addPlayer(Player p){
        players[playerCounter++] = p;
    }
    public Player[] getPlayers(){
        return players;
    }
    public void updateNumberOfPlayers(){
        if(numberOfPlayer==1){
            players = swapArray(players,numberOfPlayer);
            playerCounter--;
        }
    }
    public void setMode(Mode m){
        mode = m;
    }
    public Mode getMode(){
        return mode;
    }
    private Player[] swapArray(Player [] bigArr, int n){
        Player [] smaArr = new Player[n];
        System.arraycopy(bigArr, 0, smaArr, 0, n);
        return smaArr;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nm.ui;

import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import static nm.Nm_Constants.*;
import nm.data.Mode;

/**
 *
 * @author Thierno
 */
public class Gui {

    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "neur_makerStyle.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String RAISED_BOX = "dark-blue";
    static final String CLASS_BORDERED_PANE2 = "bordered_pane2";
    static final String CLASS_SUBJECT_PANE = "subject_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String EMPTY_TEXT = "";
    private final Stage primaryStage;
    private StackPane nmPane;
    private Scene primaryScene;
    private MenuBar menuBar;
    private Pane menuBarsPane;
    private Button letterButton;
    private Button numberButton;
    private Button bothLN;
    private BorderPane playSpacePane;
    private Label playerNameLabel;
    private Label scoreLabel;
    private HBox questionArea;
    private HBox choiceButtons;
    private Button voyelButton;
    private Button consonantButton;
    private HBox answerBox;
    private TextField answerField;
    private Button enterButton;
    private HBox bottomToolButton;
    private Button alphabetButton;
    private Button digitButton;
    private Label answerLabel;

    public Gui(Stage initPrimaryStage) {
        primaryStage = initPrimaryStage;
    }

    public void initGUI(String appTitle) {
        /*initDialogs();
         initFileToolbar();
        
         initWorkspace();
         initEventHandlers();*/
        initWindow(appTitle);
        initEventHandlerS();
    }

    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);
        // primaryStage.setIconified(true);
        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        System.out.println("X= " + bounds.getWidth());
        System.out.println("Y= " + bounds.getHeight());
        primaryStage.setHeight(bounds.getHeight());

        // System.exit(0);
        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        nmPane = new StackPane();
        String imagePath = "file:" + PATH_ARTWORK + "HomeImage.jpg";
        Image screenImage = new Image(imagePath);
        ImageView view = new ImageView(screenImage);
        nmPane.getChildren().add(view);
        initNorthToolManuBars();
        nmPane.getChildren().add(menuBarsPane);

        HBox options = new HBox();
        options.setTranslateY(450);
        options.setTranslateX(200);
        //options.getStyleClass().add(RAISED_BOX);
        options.setSpacing(20.0);

        String im1 = "file:" + PATH_ARTWORK + "LettersRed.jpg";

        String im2 = "file:" + PATH_ARTWORK + "mathematics.jpg";
        
        String im3 = "file:" + PATH_ARTWORK + "LetterDigit.jpg";

         letterButton = initChildButton(options, im1, "Letters", false);
         numberButton = initChildButton(options, im2, "Chiffres", false);
         bothLN = initChildButton(options, im3, "Letters and Digits", false);
        
        //letterButton.setBackground(Background.EMPTY);
        //letterButton.getStyleClass().add(RAISED_BOX);
        //numberButton.getStyleClass().add(RAISED_BOX);

        nmPane.getChildren().add(options);

          //nmPane.setCenter(options);
        /*wdkPane.setTop(fileToolbarPane);
        
         String imagePath2 = "file:" + PATH_IMAGES+"Robinson-Cano.gif";
         Image screenImage2  = new Image(imagePath2);
         ImageView view2 = new ImageView(screenImage2);
         String imagePath = "file:" + PATH_IMAGES+"HomeScreen.gif";
         Image screenImage  = new Image(imagePath);
         ImageView view = new ImageView(screenImage);
         HBox box = new HBox();
         box.getChildren().addAll(view2, view);
        
         wdkPane.setCenter(view2);
         */
       // nmPane.getStyleClass().add(CLASS_SUBJECT_PANE);
        //nmPane.getCenter().getStyleClass().add(CLASS_SUBJECT_PANE);
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene = new Scene(nmPane);
        primaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET);

        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }
    private void initPlaySpace(){
       playSpacePane = new BorderPane();
       
        VBox playAreaBox = new VBox();
        playerNameLabel = creatLabel("", CLASS_SUBHEADING_LABEL);
        scoreLabel = creatLabel("", CLASS_SUBHEADING_LABEL);
        questionArea = new HBox();
        choiceButtons = new HBox();
        voyelButton =  initChildButton(choiceButtons, VOWEL, VOWEL, false);
        consonantButton =  initChildButton(choiceButtons, CONSONANT, CONSONANT, false);
        
        answerBox = new HBox();
        
        answerField = new TextField();
        answerBox.getChildren().add(answerField);
        enterButton = initChildButton(answerBox,ENTER, ENTER, false);
        answerLabel = creatLabel("", CLASS_SUBHEADING_LABEL); 
        bottomToolButton = new HBox();
        alphabetButton = initChildButton(bottomToolButton,ALPHABET, ALPHABET_TOOLTIP, false);
        digitButton = initChildButton(bottomToolButton,DIGIT, DIGIT_TOOLTIP, false);
        playAreaBox.getChildren().addAll(playerNameLabel,scoreLabel,questionArea,
                choiceButtons,answerBox,answerLabel,bottomToolButton);
    }
    private Label creatLabel(String lble, String style) {
        Label label = new Label(lble);
        label.getStyleClass().add(style);
        return label;
    }

    private Button initChildButton(Pane toolbar, String imagePath, String tooltip, boolean disabled) {
        //PropertiesManager props = PropertiesManager.getPropertiesManager();
        // String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Button button = new Button();
        button.setDisable(disabled);
        Image buttonImage;
        if (isPath(imagePath)) {
            buttonImage = new Image(imagePath);
            button.setGraphic(new ImageView(buttonImage));
        } else {
            button.setText(imagePath);
        }
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        button.getStyleClass().add(RAISED_BOX);
        toolbar.getChildren().add(button);
        return button;
    }

    private void initNorthToolManuBars() {
        menuBarsPane = new Pane();
        menuBar = new MenuBar();
        Menu menuFile = new Menu("File");

        // --- Menu Edit
        Menu menuOptions = new Menu("Options");
        menuBar.setTranslateY(0.0);
        menuBar.setTranslateX(0.0);
        // --- Menu View
        Menu menuHelp = new Menu("Help");
        Menu menuAbout = new Menu("About");
        menuBar.getMenus().addAll(menuFile, menuOptions, menuHelp, menuAbout);
        menuBarsPane.getChildren().add(menuBar);
        menuBarsPane.setTranslateX(0.0);
        menuBarsPane.setTranslateY(0.0);
    }
    private void initOptionsMenu(){
        MenuItem optionMenu = new MenuItem();
        
    }
    
    private boolean isPath(String str) {
        if (str.contains("/")) {
            return true;
        }
        return false;
    }
    private void initEventHandlerS(){
        letterButton.setOnAction(e->{
            new Options(primaryStage, Mode.LETTER);
           
        }); 
        numberButton.setOnAction(e->{
            new Options(primaryStage, Mode.DIGIT);
           
        }); 
        bothLN.setOnAction(e->{
            new Options(primaryStage, Mode.BOTH);
           
        }); 
    }
}

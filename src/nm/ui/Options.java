/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nm.ui;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.Modality;
import nm.data.Mode;
import nm.data.Player;
import nm.data.Session;
import static nm.ui.Gui.CLASS_HEADING_LABEL;
import static nm.ui.Gui.CLASS_PROMPT_LABEL;
import static nm.ui.Gui.PRIMARY_STYLE_SHEET;
import static nm.ui.Gui.RAISED_BOX;

/**
 *
 * @author Thierno
 */
public class Options extends Stage {

    private GridPane gridPane;
    private ComboBox<Integer> numberPlayerComboBox;
    private ComboBox<Integer> timeComboBox;
    private int numbersPlayer;
    private TextField[] playersNamesFields;
    private Label[] playerLabels;
    private RadioButton alternateRadio;
    Stage primaryStage;
    Session session;
    Player player;
    private Label timeLabel;
    private Label alternateLabel;
    private Label numPlayersLabel;
    private Label settupLabel;
    private Button okButton;
    private Button cancelButton;
    private Scene dialogScene;
    private final Mode mode;

    public Options(Stage primaryStage, Mode m) {
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        numbersPlayer = 1;
        // this.player = null;
        //this.session = null;
        mode = m;
        session = new Session(2, mode);
        initOptions();
        initEventHandlers();
        this.showAndWait();
    }

    private void initOptions() {
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        settupLabel = creatLabel("Settup", CLASS_HEADING_LABEL);
        numPlayersLabel = creatLabel("Number of Player: ", CLASS_PROMPT_LABEL);
        numberPlayerComboBox = new ComboBox<>();
        numberPlayerComboBox.getItems().addAll(1, 2);
        /*numberPlayerComboBox.setOnAction(e -> {
            session = new Session(numberPlayerComboBox.getSelectionModel().getSelectedItem());
            numbersPlayer = session.getNumberPlayer();
        });*/

        timeLabel = creatLabel("Time (in s): ", CLASS_PROMPT_LABEL);
        timeComboBox = new ComboBox();
        playersNamesFields = new TextField[2];

        playerLabels = new Label[2];
        for (int i = 0; i < 2; i++) {

            playerLabels[i] = creatLabel("Name of Player " + (i + 1) + ": ", CLASS_PROMPT_LABEL);
            playersNamesFields[i] = new TextField();
            session.addPlayer(new Player());
        }
       
        playersNamesFields[0].textProperty().addListener((observable, oldValue, newValue) -> {
            session.getPlayers()[0].setName(newValue);
        });
        playersNamesFields[1].textProperty().addListener((observable, oldValue, newValue) -> {
            session.getPlayers()[1].setName(newValue);
        });
        
         playersNamesFields[1].setDisable(true);
        //alternateLabel = creatLabel("Alternate",CLASS_PROMPT_LABEL);
        alternateRadio = new RadioButton("Alternate");
        alternateRadio.getStyleClass().add(CLASS_PROMPT_LABEL);
        timeComboBox.getItems().addAll(30, 60, 120, 150, 1000);
        alternateRadio.setDisable(true);
        
        okButton = new Button("OK");
        cancelButton = new Button("Cancel");

        okButton.getStyleClass().add(RAISED_BOX);
        cancelButton.getStyleClass().add(RAISED_BOX);

        gridPane.add(settupLabel, 0, 0, 2, 1);
        gridPane.add(numPlayersLabel, 0, 1, 1, 1);
        gridPane.add(numberPlayerComboBox, 1, 1, 1, 1);
        gridPane.add(playerLabels[0], 0, 2, 1, 1);
        gridPane.add(playersNamesFields[0], 1, 2, 1, 1);
        gridPane.add(playerLabels[1], 0, 3, 1, 1);
        gridPane.add(playersNamesFields[1], 1, 3, 1, 1);
        gridPane.add(alternateRadio, 0, 4, 1, 1);
        gridPane.add(timeLabel, 0, 5, 1, 1);
        gridPane.add(timeComboBox, 1, 5, 1, 1);
        gridPane.add(okButton, 0, 6, 1, 1);
        gridPane.add(cancelButton, 1, 6, 1, 1);

        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);

    }

    private void initEventHandlers() {
        alternateRadio.setOnAction(e -> {
            session.setAlternate(alternateRadio.isSelected());
            if (alternateRadio.isSelected() == false) {
                timeComboBox.setDisable(true);
            } else {
                timeComboBox.setDisable(false);
            }
        });
        numberPlayerComboBox.setOnAction(e -> {
            session.setNumberPlayer(numberPlayerComboBox.getSelectionModel().getSelectedItem());
            numbersPlayer = session.getNumberPlayer();
            if (numbersPlayer <2) {
                playersNamesFields[1].setDisable(true);
                alternateRadio.setDisable(true);
            } else {
                playersNamesFields[1].setDisable(false);
                alternateRadio.setDisable(false);
            }
        });

    }

    private Label creatLabel(String lble, String style) {
        Label label = new Label(lble);
        label.getStyleClass().add(style);
        return label;
    }
}
